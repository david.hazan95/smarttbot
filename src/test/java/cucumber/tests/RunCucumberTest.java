package cucumber.tests;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


//classe runner do cucumber
@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty"},
//localizacao das features
				features = "src/test/resources/cucumber/features",
//localizacao dos steps, como estao na mesma pasta, nao foi necessario
				glue = "")
public class RunCucumberTest {
}
