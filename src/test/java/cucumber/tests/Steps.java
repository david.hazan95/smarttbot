package cucumber.tests;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.After;
import cucumber.api.java.es.Dado;
import cucumber.api.java.it.Quando;
import cucumber.api.java.pt.Entao;
import cucumber.cases.Cases;
import cucumber.utils.Navegador;
import cucumber.utils.Usuarios;

public class Steps {
	WebDriver driver;
	WebDriverWait wait;
	
	@Dado("que estou na tela de login")
	public void que_estou_na_tela_de_login() {
		//configuro para rodar google chorme de acordo o OS
		Navegador.getOS();
		//Falo para o chrome abrir em modo tela cheia, poderia ser o normal, mas 
		//encontrei um problema com o modo responsivo e nao quis demorar
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-fullscreen");
		driver = new ChromeDriver(options);
		
		//configuro o wait para dar timeout depois de 15 segundos
		wait = new WebDriverWait(driver,15);
		
		//abro no site da smarttbot
		driver.get("https://smarttbot.com/");
	}

	@Quando("realizo login")
	public void realizo_login() {
		
		Cases.acessarLogin(driver);
		
		Cases.preencherUsuario(driver, wait, Usuarios.USERNAME.getValor());
		
		Cases.preencherSenha(driver, Usuarios.PASSWORD.getValor());
		
		Cases.realizarLogin(driver);
	}

	@Quando("crio robo hidra")
	public void crio_robo_hidra() throws InterruptedException{
		Cases.clicarRoboMenu(driver, wait);
		
		Cases.criarRobo(driver);
		
		Cases.selecionarEstrategia(driver, wait);
		
		Cases.selecionarInstanciaSimulada(driver, wait);
		
		Cases.preencherDadosRobo(driver, wait);
	}

	@Quando("preencho parametros do robo")
	public void preencho_parametros_do_robo() {
		Cases.salvarParametros(driver, wait);
	}

	@Entao("observo mensagem de sucesso")
	public void observo_mensagem_de_sucesso() {
		Cases.verificarMensagemSucesso(driver, wait);
	}
	
	
	//eliminar instancia do chrome depois de testar
	@After
	public void breakDown() {
		driver.quit();
	}
}
