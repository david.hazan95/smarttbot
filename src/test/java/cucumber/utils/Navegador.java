package cucumber.utils;

public class Navegador {

	
	// metodo para pegar a OS do computador rodando e definir o chromedrive especifico
	public static void getOS() {
		String osName = System.getProperty("os.name");
		System.out.println(osName);
		if(osName.startsWith("Windows")) {
			System.setProperty("webdriver.chrome.driver", "./src/test/resources/driver/chromedriver-windows.exe");
		}else if(osName.startsWith("Mac")) {
			System.setProperty("webdriver.chrome.driver", "./src/test/resources/driver/chromedriver-mac");
		}else {
			System.setProperty("webdriver.chrome.driver", "./src/test/resources/driver/chromedriver-linux");
		}
	}
}
