package cucumber.cases;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//classe para armazenar acoes atomicas, usado principalmente para reutilizacao de codigo
public class Cases {
	public static void acessarLogin(WebDriver driver) {
		
		WebElement menuLogin = driver.findElement(By.xpath(
				"//li[@id='menu-item-422']//span[@class='menu-text'][contains(text(),'Login')]"));
		menuLogin.click();
	}
	
	public static void preencherUsuario(WebDriver driver, WebDriverWait wait,String usuario) {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(
				"login-username")));
		
		WebElement username = driver.findElement(By.id("login-username"));
		username.sendKeys(usuario);
	}
	
	public static void preencherSenha(WebDriver driver, String senha) {
		WebElement password = driver.findElement(By.id("login-password"));
		password.sendKeys(senha);
	}
	
	public static void realizarLogin(WebDriver driver) {
		WebElement loginButton = driver.findElement(By.id("login-button"));
		loginButton.click();
	}

	public static void clicarRoboMenu(WebDriver driver, WebDriverWait wait){
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(
				"sidebar-menu-item-robots")));
		WebElement robotButton = driver.findElement(By.id(
				"sidebar-menu-item-robots"));
		robotButton.click();
	}
	
	public static void criarRobo(WebDriver driver){
		WebElement createRobot = driver.findElement(By.xpath(
				"//*[contains(@class,'svg-inline--fa fa-plus fa-w-14 fa-lg')]"));
		createRobot.click();
	}
	
	public static void selecionarEstrategia(WebDriver driver, WebDriverWait wait){
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(
				"select-strategy-260")));
		WebElement strategy = driver.findElement(By.id("select-strategy-260"));
		strategy.click();
	}
	
	public static void selecionarInstanciaSimulada(WebDriver driver, WebDriverWait wait){
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(
				"create-instance-simulated")));
		WebElement mode = driver.findElement(By.id("create-instance-simulated"));
		mode.click();
	}
	
	public static void preencherDadosRobo(WebDriver driver, WebDriverWait wait){
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(
				"new_instance-name")));
		WebElement robotName = driver.findElement(By.id("new_instance-name"));
		robotName.sendKeys("Robo Teste");
		
		WebElement nextStep = driver.findElement(By.id("next-step-button"));
		nextStep.click();
	}

	public static void salvarParametros(WebDriver driver, WebDriverWait wait) {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("notification-message")));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(@class,'svg-inline--fa fa-save fa-w-14 fa-sm')]")));
		
		WebElement save = driver.findElement(By.xpath("//*[contains(@class,'svg-inline--fa fa-save fa-w-14 fa-sm')]"));
		save.click();
		
		WebElement confirm = driver.findElement(By.xpath("//span[contains(text(),'Salvar')]"));
		confirm.click();
	}

	public static void verificarMensagemSucesso(WebDriver driver, WebDriverWait wait) {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("notification-title")));
		assertEquals(driver.findElement(By.className("notification-title")).getText(), "Sucesso!");
	}
	
}
