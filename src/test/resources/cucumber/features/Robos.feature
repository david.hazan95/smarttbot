#language : pt
	Funcionalidade: Fluxo de criar um robo e salvar parametros
		
		Cenario: Criar robo e salvar parametros
			Dado que estou na tela de login
			Quando realizo login
			E crio robo hidra
			E preencho parametros do robo
			Entao observo mensagem de sucesso
